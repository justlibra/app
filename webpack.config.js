const path = require("path");
const HTMLWebpackPlugin = require("html-webpack-plugin");
const AsyncChunkNames = require("webpack-async-chunk-names-plugin");
const { CleanWebpackPlugin } = require("clean-webpack-plugin");

module.exports = {
  // webpack optimization mode
  mode: process.env.NODE_ENV ? process.env.NODE_ENV : "development",

  // entry file(s)
  entry: "./src/index.js",

  // output file(s) and chunks
  output: {
    path: path.resolve(__dirname, "dist"),
    filename: "main.js",
    chunkFilename: "[name].js",
    publicPath: "/"
  },

  // module/loaders configuration
  module: {
    rules: [
      {
        test: /\.js|jsx?$/,
        exclude: /node_modules/,
        loader: "babel-loader",
        query: {
          presets: ["@babel/preset-env", "@babel/preset-react"]
        }
      },
      {
        test: /\.css$/,
        use: ["style-loader", "css-loader"]
      },
      {
        test: /\.(png|svg|jpg|jpeg|gif|ico)$/,
        exclude: /node_modules/,
        use: ["file-loader?name=[name].[ext]"]
      },
      {
        test: /\.json$/,
        exclude: /(node_modules)/,
        use: "json-loader"
      }
    ]
  },

  plugins: [
    new CleanWebpackPlugin(),
    new HTMLWebpackPlugin({
      template: path.resolve(__dirname, "public/index.html"),
      filename: "index.html"
    })
  ],

  performance: {
    hints: false
  },

  // development server configuration
  devServer: {
    inline: true,
    port: 3000,
    historyApiFallback: true
  },

  // generate source map
  devtool:
    "production" === process.env.NODE_ENV
      ? "source-map"
      : "cheap-module-eval-source-map",

  // optimization
  optimization: {
    splitChunks: {
      cacheGroups: {
        default: false,
        vendors: false,

        // vendor chunk
        vendor: {
          // name of the chunk
          name: "vendor",

          // async + async chunks
          chunks: "all",

          // import file path containing node_modules
          test: /node_modules/,

          // priority
          priority: 20
        },

        // common chunk
        common: {
          name: "common",
          minChunks: 2,
          chunks: "all",
          priority: 10,
          reuseExistingChunk: true,
          enforce: true
        }
      }
    }
  }
};
