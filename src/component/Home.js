import React from "react";
import "../App.css";
import "../style.css";
import {
  BrowserRouter,
  Switch,
  NavLink as Link,
  Route
} from "react-router-dom";
import loadable from "react-loadable";
import { ListProgram } from "./dummy";

const LoadingComponent = () => <h3>please wait...</h3>;

// const MiniProgram1 = loadable({
//   loader: () => import("./MiniProgram1"),
//   loading: LoadingComponent
// });

// const MiniProgram2 = loadable({
//   loader: () => import("./MiniProgram2"),
//   loading: LoadingComponent
// });

export class Home extends React.Component {
  componentDidMount() {
    if (ListProgram.length > 0) {
      var modules = [];
      ListProgram.map((m, index) => {
        const url = "/" + m;
        return modules.push({
          path: url,
          moduleID: m,
          modules: loadable({
            loader: () => import("./" + m),
            loading: LoadingComponent
          })
        });
      });
      this.setState({ list: modules });
    }
  }

  render() {
    return (
      <div className="App">
        <header className="App-header">
          <h3>Welcome to my store</h3>
          <div>
            <BrowserRouter>
              <div>
                <div>
                  {this.state &&
                    this.state.list &&
                    this.state.list.map(item => {
                      return (
                        <Link
                          key={item.moduleID}
                          to={item.path}
                          activeClassName="active"
                        >
                          {item.moduleID}
                        </Link>
                      );
                    })}
                </div>

                <Switch>
                  {this.state &&
                    this.state.list &&
                    this.state.list.map(item => {
                      return (
                        <Route
                          exact
                          key={item.moduleID}
                          path={item.path}
                          component={item.modules}
                        />
                      );
                    })}
                </Switch>
              </div>
            </BrowserRouter>
          </div>
        </header>
      </div>
    );
  }
}

export default Home;
